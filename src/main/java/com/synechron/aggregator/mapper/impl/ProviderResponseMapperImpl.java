package com.synechron.aggregator.mapper.impl;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.synechron.aggregator.dto.PlanDetailsDTO;
import com.synechron.aggregator.dto.ResponseStructureDTO;
@Component
public class ProviderResponseMapperImpl {

	public PlanDetailsDTO mapResponseToPlanDetail(Map<String, Object> response,
			ResponseStructureDTO responseStructure) {
		PlanDetailsDTO planDetailsDTO = new PlanDetailsDTO();
		response.forEach((k, v) -> {
			if (k.equals(responseStructure.getPlanId()))
				planDetailsDTO.setPlanId((Integer) v);
			else if (k.equals(responseStructure.getPlanName()))
				planDetailsDTO.setPlanName((String) v);
			else if (k.equals(responseStructure.getPremuimYearly()))
				planDetailsDTO.setPremuimYearly((Integer) v);
			else if (k.equals(responseStructure.getMaturityAge()))
				planDetailsDTO.setMaturityAge((Integer) v);

		});
		return planDetailsDTO;
	}

}
