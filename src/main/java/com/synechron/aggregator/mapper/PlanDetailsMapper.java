package com.synechron.aggregator.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PlanDetailsMapper {

	/*
	 * @Mappings({ @Mapping(source = responseStructureDTO.ge, target = "planId") })
	 * PlanDetailsDTO covertObjectToPlanDetailDTO(@Source Object object,
	 * ResponseStructureDTO responseStructureDTO);
	 */
}
