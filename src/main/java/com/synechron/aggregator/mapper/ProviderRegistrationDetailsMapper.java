package com.synechron.aggregator.mapper;

import java.util.List;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import com.synechron.aggregator.dto.ProviderRegistrationDetailsDTO;
import com.synechron.aggregator.entity.ProviderRegistrationDetails;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface ProviderRegistrationDetailsMapper {
	
	ProviderRegistrationDetailsDTO convertEntityToDTO(ProviderRegistrationDetails providerRegistrationDetails);

	@InheritConfiguration(name = "convertEntityToDTO")
	ProviderRegistrationDetails convertDTOToEntity(ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO);

	List<ProviderRegistrationDetailsDTO> convertEntityListToDTOList(List<ProviderRegistrationDetails> providerRegistrationDetails);

	@InheritConfiguration(name = "convertEntityListToDTOList")
	List<ProviderRegistrationDetails> convertDTOListToEntityList(List<ProviderRegistrationDetailsDTO> providerRegistrationDetailsDTOs);

}
