package com.synechron.aggregator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.aggregator.dto.ProviderRegistrationDetailsDTO;
import com.synechron.aggregator.dto.ResponseDTO;
import com.synechron.aggregator.service.ProviderRegistrationService;

@RestController
@RequestMapping("aggregator/api/v1")
public class ProviderRegistrationController {

	@Autowired
	private ProviderRegistrationService providerRegistrationService;

	@PostMapping("/providers/registration")
	public ResponseDTO registerProvider(@RequestBody ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO) {

		ProviderRegistrationDetailsDTO registered = providerRegistrationService
				.regsiterProvider(providerRegistrationDetailsDTO);
		if (registered != null) {
			return ResponseDTO.builder().isSuccess(true).message("SUCCESS").statusCode(200).data(registered).build();

		} else {
			return ResponseDTO.builder().isSuccess(false).message("BAD_REQUEST").statusCode(400).build();
		}
	}

	@PutMapping("/providers/registration")
	public ResponseDTO updateRegistration(@RequestBody ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO) {

		ProviderRegistrationDetailsDTO updated = providerRegistrationService
				.updateProviderRegistration(providerRegistrationDetailsDTO);
		if (updated != null) {
			return ResponseDTO.builder().isSuccess(true).message("SUCCESS").statusCode(200).data(updated).build();

		} else {
			return ResponseDTO.builder().isSuccess(false).message("BAD_REQUEST").statusCode(400).build();
		}
	}

	@GetMapping("/providers/registration")
	public ResponseDTO retrieveAllRegistrtions() {
		List<ProviderRegistrationDetailsDTO> providerRegistrationDetailsDTOs = providerRegistrationService
				.retrieveAllRegistrations();
		if (!CollectionUtils.isEmpty(providerRegistrationDetailsDTOs)) {
			return ResponseDTO.builder().isSuccess(true).message("SUCCESS").statusCode(200)
					.data(providerRegistrationDetailsDTOs).build();

		} else {
			return ResponseDTO.builder().isSuccess(false).message("No Record found").statusCode(404).build();

		}
	}

	@GetMapping("/providers/registrationById/{registrationId}")
	public ResponseDTO retrieveRegistrationById(@PathVariable Integer registrationId) {
		ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO = providerRegistrationService
				.retrieveByRegistratinById(registrationId);
		if (providerRegistrationDetailsDTO != null) {
			return ResponseDTO.builder().isSuccess(true).message("SUCCESS").statusCode(200)
					.data(providerRegistrationDetailsDTO).build();

		} else {
			return ResponseDTO.builder().isSuccess(false).message("No Record Found").statusCode(404).build();
		}
	}

}
