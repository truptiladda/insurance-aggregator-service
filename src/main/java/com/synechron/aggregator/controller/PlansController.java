package com.synechron.aggregator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.aggregator.dto.PlanDetailsDTO;
import com.synechron.aggregator.dto.ResponseDTO;
import com.synechron.aggregator.service.PlansService;

@RestController
@RequestMapping("aggregator/api/v1")
public class PlansController {

	@Autowired
	private PlansService planService;

	@GetMapping("/plans")
	public ResponseDTO retrievePlansFromRegisteredProviders() {
		List<PlanDetailsDTO> planDetailsDTOs = planService.retrievePlansFromRegisteredProviders();
		if (CollectionUtils.isEmpty(planDetailsDTOs)) {
			return ResponseDTO.builder().isSuccess(true).statusCode(404).message("No recordfound").build();
		} else {
			return ResponseDTO.builder().isSuccess(true).statusCode(200).message("SUCCESS").data(planDetailsDTOs)
					.build();
		}

	}

}
