package com.synechron.aggregator.service;

import java.util.List;

import com.synechron.aggregator.dto.PlanDetailsDTO;

public interface PlansService {

	List<PlanDetailsDTO> retrievePlansFromRegisteredProviders();

}
