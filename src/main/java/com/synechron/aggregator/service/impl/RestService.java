package com.synechron.aggregator.service.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.synechron.aggregator.dto.ProviderRegistrationDetailsDTO;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RestService {

	public Map<String, Object> invoiceRestApi(ProviderRegistrationDetailsDTO url,
			MultiValueMap<String, String> queryParams, HttpMethod httpMethodName, Object postObject) {
		RestTemplate restTemplate = new RestTemplate();

		URI uri;
		ResponseEntity<?> response;
		HttpEntity<?> httpEntity = new HttpEntity<>(new HashMap<>());
		uri = UriComponentsBuilder.fromHttpUrl(url.getProviderUrl()).queryParams(queryParams).build().encode().toUri();
		try {
			if (url.getResponseType().equals("JSON")) {

				response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Map.class);
				return (Map<String, Object>) response.getBody();
			} else if (url.getResponseType().equals("XML")) {
				/*
				 * ResponseEntity<String> xmlResponse = restTemplate.exchange(uri,
				 * HttpMethod.GET, httpEntity, String.class); XmlMapper mapper = new
				 * XmlMapper(); XMLRespnseDTO value = mapper.readValue(xmlResponse.getBody(),
				 * XMLRespnseDTO.class); ObjectMapper oMapper = new ObjectMapper(); List<Object>
				 * map = oMapper.convertValue(value, List.class); return null;
				 */

			}
		} catch (Exception e) {
			log.error("Exception in calling api: " + url + " " + e.getMessage());
//			return null;

		}

		return null;

	}

}
