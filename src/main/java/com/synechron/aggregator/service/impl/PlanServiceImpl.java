package com.synechron.aggregator.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synechron.aggregator.dto.PlanDetailsDTO;
import com.synechron.aggregator.dto.ProviderRegistrationDetailsDTO;
import com.synechron.aggregator.dto.ResponseStructureDTO;
import com.synechron.aggregator.mapper.impl.ProviderResponseMapperImpl;
import com.synechron.aggregator.service.PlansService;
import com.synechron.aggregator.service.ProviderRegistrationService;

@Service
public class PlanServiceImpl implements PlansService {

	@Autowired
	private ProviderRegistrationService providerRegistrationService;

	@Autowired
	private ProviderResponseMapperImpl mapper;

	@Autowired
	private RestService restService;

	@Override
	public List<PlanDetailsDTO> retrievePlansFromRegisteredProviders() {
		List<PlanDetailsDTO> planDetailsDTOs = new ArrayList<>();

		List<ProviderRegistrationDetailsDTO> providerRegistrationDetailsDTOs = providerRegistrationService
				.retrieveAllRegistrations();

//		ForkJoinPool customPool = new ForkJoinPool(providerRegistrationDetailsDTOs.size());
//		customPool.submit(() ->
		providerRegistrationDetailsDTOs.parallelStream().forEach(registration -> {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				ResponseStructureDTO responseStructureDTO = objectMapper.readValue(registration.getResponseStructure(),
						ResponseStructureDTO.class);
				Map<String, Object> response = restService.invoiceRestApi(registration, null, HttpMethod.GET, null);
				if (registration.getDataKey() != null) {
					List<Map<String, Object>> plans = (List<Map<String, Object>>) response
							.get(registration.getDataKey());
					plans.forEach(plan -> {
						PlanDetailsDTO planDetailsDTO = mapper.mapResponseToPlanDetail(plan, responseStructureDTO);
						planDetailsDTOs.add(planDetailsDTO);
					});
				} else {
//					List<Map<String, Object>> plans = response.
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});

		return planDetailsDTOs;
	}

}
