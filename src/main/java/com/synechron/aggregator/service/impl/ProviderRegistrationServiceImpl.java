package com.synechron.aggregator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.aggregator.dto.ProviderRegistrationDetailsDTO;
import com.synechron.aggregator.entity.ProviderRegistrationDetails;
import com.synechron.aggregator.mapper.ProviderRegistrationDetailsMapper;
import com.synechron.aggregator.repository.ProviderRegistrationDetailRepository;
import com.synechron.aggregator.service.ProviderRegistrationService;

@Service
public class ProviderRegistrationServiceImpl implements ProviderRegistrationService {

	@Autowired
	private ProviderRegistrationDetailRepository providerRegistrationDetailRepository;

	@Autowired
	private ProviderRegistrationDetailsMapper providerRegistrationMapper;

	@Override
	public ProviderRegistrationDetailsDTO retrieveByRegistratinById(Integer registrationId) {
		ProviderRegistrationDetails providerRegistrationDetails = providerRegistrationDetailRepository
				.findByRegistrationId(registrationId);
		return providerRegistrationMapper.convertEntityToDTO(providerRegistrationDetails);
	}

	@Override
	public List<ProviderRegistrationDetailsDTO> retrieveAllRegistrations() {
		List<ProviderRegistrationDetails> providerRegistrationDetails = providerRegistrationDetailRepository.findAll();
		return providerRegistrationMapper.convertEntityListToDTOList(providerRegistrationDetails);
	}

	@Override
	public ProviderRegistrationDetailsDTO regsiterProvider(
			ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO) {
		ProviderRegistrationDetails providerRegistrationDetails = providerRegistrationMapper
				.convertDTOToEntity(providerRegistrationDetailsDTO);
		ProviderRegistrationDetails saved = providerRegistrationDetailRepository.save(providerRegistrationDetails);
		return providerRegistrationMapper.convertEntityToDTO(saved);
	}

	@Override
	public ProviderRegistrationDetailsDTO updateProviderRegistration(
			ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO) {
		ProviderRegistrationDetails providerRegistrationDetails = providerRegistrationDetailRepository
				.findByRegistrationId(providerRegistrationDetailsDTO.getRegistrationId());
		if (providerRegistrationDetails != null) {
			providerRegistrationDetails = providerRegistrationMapper.convertDTOToEntity(providerRegistrationDetailsDTO);
			providerRegistrationDetails = providerRegistrationDetailRepository.save(providerRegistrationDetails);

			return providerRegistrationMapper.convertEntityToDTO(providerRegistrationDetails);
		}
		return null;
	}

	@Override
	public void removeRegistrationById(Integer registrationId) {

		ProviderRegistrationDetails providerRegistrationDetails = providerRegistrationDetailRepository
				.findByRegistrationId(registrationId);
		providerRegistrationDetailRepository.delete(providerRegistrationDetails);

	}

}
