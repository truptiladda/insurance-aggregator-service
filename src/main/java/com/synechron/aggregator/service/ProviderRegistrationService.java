package com.synechron.aggregator.service;

import java.util.List;

import com.synechron.aggregator.dto.ProviderRegistrationDetailsDTO;

public interface ProviderRegistrationService {

	ProviderRegistrationDetailsDTO retrieveByRegistratinById(Integer registrationId);

	List<ProviderRegistrationDetailsDTO> retrieveAllRegistrations();

	ProviderRegistrationDetailsDTO regsiterProvider(ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO);

	ProviderRegistrationDetailsDTO updateProviderRegistration(
			ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO);

	void removeRegistrationById(Integer registrationId);

}
