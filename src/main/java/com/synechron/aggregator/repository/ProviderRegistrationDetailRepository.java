package com.synechron.aggregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.synechron.aggregator.entity.ProviderRegistrationDetails;

@Repository
public interface ProviderRegistrationDetailRepository extends JpaRepository<ProviderRegistrationDetails, Integer> {

	ProviderRegistrationDetails findByRegistrationId(@Param("registrationId") Integer registrationId);
}
