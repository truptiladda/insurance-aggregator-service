package com.synechron.aggregator.exception;

import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;

import com.synechron.aggregator.dto.ResponseDTO;

@ControllerAdvice
public class AggregatorGlobalExceptionHandler {

	@ExceptionHandler({ InvalidDataAccessResourceUsageException.class })
	public ResponseEntity<ResponseDTO> handleInvalidDataAccessResourceUsageException(
			InvalidDataAccessResourceUsageException e) {

		ResponseDTO responseDTO = ResponseDTO.builder().isSuccess(false).statusCode(400)
				.errorMessage("Invalid Data type").build();
		return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler({ ResourceAccessException.class })
	public ResponseEntity<ResponseDTO> handleResourceAccessException(ResourceAccessException e) {

		ResponseDTO responseDTO = ResponseDTO.builder().isSuccess(false).statusCode(400)
				.errorMessage("Some I/O Error occured").build();
		return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler({ RuntimeException.class })
	public ResponseEntity<ResponseDTO> handleRunTimeException(RuntimeException e) {

		ResponseDTO responseDTO = ResponseDTO.builder().isSuccess(false).statusCode(500)
				.errorMessage("Internal Server Error Occured").build();
		return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);

	}
}
