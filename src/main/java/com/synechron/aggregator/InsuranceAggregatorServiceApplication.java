package com.synechron.aggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceAggregatorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsuranceAggregatorServiceApplication.class, args);
	}

}
