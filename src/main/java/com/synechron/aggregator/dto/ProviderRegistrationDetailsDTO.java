package com.synechron.aggregator.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProviderRegistrationDetailsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer registrationId;

	private String providerName;

	private String providerUrl;

	private String responseType;

	private String responseStructure;

	private String dataKey;

}
