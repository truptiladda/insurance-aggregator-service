package com.synechron.aggregator.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PlanDetailsDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer planId;

	private String planName;

	private Integer premuimYearly;

	private Integer maturityAge;

}
