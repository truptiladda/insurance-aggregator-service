package com.synechron.aggregator.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ResponseDTO {

	@NotNull
	private Integer statusCode;

	@NotNull
	private Boolean isSuccess;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String message;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Object errorMessage;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Object data;

}
