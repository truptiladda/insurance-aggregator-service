package com.synechron.aggregator.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "providers_registration_details")
@Data
public class ProviderRegistrationDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "registration_id")
	private Integer registrationId;

	@Column(name = "provider_name")
	private String providerName;

	@Column(name = "provider_url")
	private String providerUrl;

	@Column(name = "response_type")
	private String responseType;

	@Column(name = "response_structure")
	private String responseStructure;

	@Column(name = "data_key")
	private String dataKey;

}
