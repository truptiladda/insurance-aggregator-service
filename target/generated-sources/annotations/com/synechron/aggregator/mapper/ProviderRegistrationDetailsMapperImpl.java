package com.synechron.aggregator.mapper;

import com.synechron.aggregator.dto.ProviderRegistrationDetailsDTO;
import com.synechron.aggregator.entity.ProviderRegistrationDetails;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-05-08T18:15:30+0530",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0 (Oracle Corporation)"
)
@Component
public class ProviderRegistrationDetailsMapperImpl implements ProviderRegistrationDetailsMapper {

    @Override
    public ProviderRegistrationDetailsDTO convertEntityToDTO(ProviderRegistrationDetails providerRegistrationDetails) {
        if ( providerRegistrationDetails == null ) {
            return null;
        }

        ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO = new ProviderRegistrationDetailsDTO();

        if ( providerRegistrationDetails.getRegistrationId() != null ) {
            providerRegistrationDetailsDTO.setRegistrationId( providerRegistrationDetails.getRegistrationId() );
        }
        if ( providerRegistrationDetails.getProviderName() != null ) {
            providerRegistrationDetailsDTO.setProviderName( providerRegistrationDetails.getProviderName() );
        }
        if ( providerRegistrationDetails.getProviderUrl() != null ) {
            providerRegistrationDetailsDTO.setProviderUrl( providerRegistrationDetails.getProviderUrl() );
        }
        if ( providerRegistrationDetails.getResponseType() != null ) {
            providerRegistrationDetailsDTO.setResponseType( providerRegistrationDetails.getResponseType() );
        }
        if ( providerRegistrationDetails.getResponseStructure() != null ) {
            providerRegistrationDetailsDTO.setResponseStructure( providerRegistrationDetails.getResponseStructure() );
        }
        if ( providerRegistrationDetails.getDataKey() != null ) {
            providerRegistrationDetailsDTO.setDataKey( providerRegistrationDetails.getDataKey() );
        }

        return providerRegistrationDetailsDTO;
    }

    @Override
    public ProviderRegistrationDetails convertDTOToEntity(ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO) {
        if ( providerRegistrationDetailsDTO == null ) {
            return null;
        }

        ProviderRegistrationDetails providerRegistrationDetails = new ProviderRegistrationDetails();

        if ( providerRegistrationDetailsDTO.getRegistrationId() != null ) {
            providerRegistrationDetails.setRegistrationId( providerRegistrationDetailsDTO.getRegistrationId() );
        }
        if ( providerRegistrationDetailsDTO.getProviderName() != null ) {
            providerRegistrationDetails.setProviderName( providerRegistrationDetailsDTO.getProviderName() );
        }
        if ( providerRegistrationDetailsDTO.getProviderUrl() != null ) {
            providerRegistrationDetails.setProviderUrl( providerRegistrationDetailsDTO.getProviderUrl() );
        }
        if ( providerRegistrationDetailsDTO.getResponseType() != null ) {
            providerRegistrationDetails.setResponseType( providerRegistrationDetailsDTO.getResponseType() );
        }
        if ( providerRegistrationDetailsDTO.getResponseStructure() != null ) {
            providerRegistrationDetails.setResponseStructure( providerRegistrationDetailsDTO.getResponseStructure() );
        }
        if ( providerRegistrationDetailsDTO.getDataKey() != null ) {
            providerRegistrationDetails.setDataKey( providerRegistrationDetailsDTO.getDataKey() );
        }

        return providerRegistrationDetails;
    }

    @Override
    public List<ProviderRegistrationDetailsDTO> convertEntityListToDTOList(List<ProviderRegistrationDetails> providerRegistrationDetails) {
        if ( providerRegistrationDetails == null ) {
            return null;
        }

        List<ProviderRegistrationDetailsDTO> list = new ArrayList<ProviderRegistrationDetailsDTO>( providerRegistrationDetails.size() );
        for ( ProviderRegistrationDetails providerRegistrationDetails1 : providerRegistrationDetails ) {
            list.add( convertEntityToDTO( providerRegistrationDetails1 ) );
        }

        return list;
    }

    @Override
    public List<ProviderRegistrationDetails> convertDTOListToEntityList(List<ProviderRegistrationDetailsDTO> providerRegistrationDetailsDTOs) {
        if ( providerRegistrationDetailsDTOs == null ) {
            return null;
        }

        List<ProviderRegistrationDetails> list = new ArrayList<ProviderRegistrationDetails>( providerRegistrationDetailsDTOs.size() );
        for ( ProviderRegistrationDetailsDTO providerRegistrationDetailsDTO : providerRegistrationDetailsDTOs ) {
            list.add( convertDTOToEntity( providerRegistrationDetailsDTO ) );
        }

        return list;
    }
}
